import org.apache.commons.lang3.math.NumberUtils;

/**
 * <p> Provides extra functionality for Apache Common Lang.</p>
 */
public class Utils {
    /**
     * <p>Checks whether {@code String} is positive number or not</p>
     *
     * <p>{@code null} and empty/blank {@code String} will return
     * {@code false}.</p>
     *
     * @param str the {@code String} to check
     * @return {@code true} if the string is a correctly formatted number
     */
    public static boolean isPositiveNumber(String str) {
        if(str == null || str.trim().length() == 0)
            return false;
        try {
            Number number = NumberUtils.createNumber(str);
            return number.longValue() > 0;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
