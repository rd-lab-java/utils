import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UtilsTest {

    @Test
    @DisplayName("Simple negative number check")
    public void testNegativeCheckA() {
        assertFalse(Utils.isPositiveNumber("-1"));
    }

    @Test
    @DisplayName("Simple negative number check")
    public void testNegativeCheckB() {
        assertFalse(Utils.isPositiveNumber("-100"));
    }

    @Test
    @DisplayName("Simple positive number check")
    public void testPositiveCheckA() {
        assertTrue(Utils.isPositiveNumber("1"));
    }

    @Test
    @DisplayName("Simple positive number check")
    public void testPositiveCheckB() {
        assertTrue(Utils.isPositiveNumber("100"));
    }

    @Test
    @DisplayName("Simple invalid number check")
    public void testInvalidNumberCheckA() {
        assertFalse(Utils.isPositiveNumber("-10aaa"));
    }

    @Test
    @DisplayName("Simple zero number check")
    public void testZeroCheck() {
        assertFalse(Utils.isPositiveNumber("0"));
    }

}